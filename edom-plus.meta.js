// ==UserScript==
// @name			eDom+
// @version			r46
// @description			Improvements to eDominations web browser game
// @author			Runy96
// @match			https://www.edominations.com/*
// @include			https://www.edominations.com
// @require			https://bitbucket.org/Rubensei/edom-plus/raw/master/jsonpath.js
// @grant			none
// @noframes
// @updateURL			https://bitbucket.org/Rubensei/edom-plus/raw/master/edom-plus.meta.js
// @downloadURL			https://bitbucket.org/Rubensei/edom-plus/raw/master/edom-plus.user.js
// ==/UserScript==