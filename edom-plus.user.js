// ==UserScript==
// @name			eDom+
// @version			r46
// @description			Improvements to eDominations web browser game
// @author			Runy96
// @match			https://www.edominations.com/*
// @include			https://www.edominations.com
// @require			https://bitbucket.org/Rubensei/edom-plus/raw/master/jsonpath.js
// @grant			none
// @noframes
// @updateURL			https://bitbucket.org/Rubensei/edom-plus/raw/master/edom-plus.meta.js
// @downloadURL			https://bitbucket.org/Rubensei/edom-plus/raw/master/edom-plus.user.js
// ==/UserScript==

function eDomPlus(){
    var t = this;
    var branch = GM_info.script.downloadURL.split('/')[6];
    var locale;
    if(location.toString().split('/').length <= 4) locale = 'en';
    else locale = location.toString().split('/')[3];
    this.data={};
    this.data.alerts=[];
    this.data.donations = {};
    this.data.globals={};
    this.data.houses=[];
    this.data.settings = {};
    this.data.stats = {};
    this.countries = {"Afghanistan":"1","Albania":"2","Algeria":"3","Argentina":"4","Armenia":"5","Australia":"6","Austria":"7","Azerbaijan":"8","Belarus":"9","Belgium":"10","Bolivia":"11","Bosnia-and-Herzegovina":"12","Brazil":"13","Bulgaria":"14","Canada":"15","Chile":"16","China":"17","Colombia":"18","Croatia":"19","Cuba":"20","Cyprus":"21","Czech-Republic":"22","Denmark":"23","Ecuador":"24","Egypt":"25","Estonia":"26","Finland":"27","France":"28","Georgia":"29","Germany":"30","Greece":"31","Hungary":"32","India":"33","Indonesia":"34","Iran":"35","Ireland":"36","Israel":"37","Italy":"38","Japan":"39","Latvia":"40","Lithuania":"41","Luxembourg":"42","Malaysia":"43","Mexico":"44","Montenegro":"45","Netherlands":"46","New-Zealand":"47","North-Korea":"48","Norway":"49","Pakistan":"50","Paraguay":"51","Peru":"52","Poland":"53","Portugal":"54","Taiwan":"55","Macedonia":"56","Moldova":"57","Romania":"58","Russia":"59","Saudi-Arabia":"60","Serbia":"61","Singapore":"62","Slovakia":"63","Slovenia":"64","South-Africa":"65","South-Korea":"66","Spain":"67","Sweden":"68","Switzerland":"69","Thailand":"70","Tunisia":"71","Turkey":"72","Ukraine":"73","United-Arab-Emirates":"74","United-Kingdom":"75","United-States":"76","Uruguay":"77","Venezuela":"78"};
    this.ed_day=parseFloat($($("span#vs219-1")[0]).text().split(" ")[1]);
    this.id = getCookie("user_id");
    this.items={"food-raw": {"id": 1, "q": [1]},"weapon-raw": {"id": 2, "q": [1]},"building-raw": {"id": 3, "q": [1]},"food": {"id": 4, "q": [1,2,3,4,5]},"weapons": {"id": 5, "q": [1,2,3,4,5]},"tank": {"id": 6, "q": [1,2,3,4,5]},"aircraft": {"id": 7, "q": [1,2,3,4,5]},"house": {"id": 8, "q": [1,2,3,4,5]},"hospital": {"id": 9, "q": [1,2,3,4,5]},"defense-system": {"id": 10, "q": [1,2,3,4,5]}};
    this.rounds=["I", "II", "III"];
    this.update_button = '<a class="vs917-2 updateBtf" style="left: 6px;" href="javascript:;"><i style="font-size: 28px; line-height: 28px; margin-left: 6px; color: white;" class="fa fa-refresh" alt=""></a>';
    this.load();
    if(!this.data.stats.day || this.data.stats.day < this.ed_day) {
        this.data.stats.day = this.ed_day;
        this.data.stats.hits = 0;
        this.data.stats.dmg = 0;
        this.store();
    }
    //Other
    $('head').append('<script async src="https://www.googletagmanager.com/gtag/js?id=UA-111679378-2"></script><script>window.dataLayer=window.dataLayer || []; function gtag(){dataLayer.push(arguments);}gtag(\'js\', new Date()); gtag(\'config\', \'UA-111679378-2\');</script>');
    $('head').append('<style>.tooltip{opacity:1!important;}.tooltip-inner{max-width:none;} .tooltip-inner td {min-width: 75px; max-width: 75px; white-space: nowrap; text-overflow: ellipsis; overflow: hidden;} .left{text-align: left;} .right{text-align: right;} #check-version:hover {}</style>');
    $('head').append('<style>#vsMsg-tab-right-text{word-break: normal;} .vs908-4 p{margin: 0px;}</style>');
    $('title').attr("orig", $('title').text());
    $('#sidebar-nav a[href="https://www.edominations.com/'+locale+'/market"]').parent().after('<li><a href="https://www.edominations.com/en/market-compare/0/0">Compare Markets</a></li>');
    $('head').append('<style>#winner{top: 43px;} @media screen and (max-width:767px){#randomEndorser button {margin-right: -3px;} #winner{top: 65px;}}</style>');
    $('head').append('<style>#edp-settings-div{width: 600px;} @media screen and (max-width:767px){#edp-settings-div {width: 70%}}</style>');
    t.showAlerts();
    t.updateTitle();
    setInterval(function(){$(".l-container-navbar").load(location.href+" .l-container-navbar>*","", function(){
        t.showAlerts();
        t.updateTitle();
    });},1000*60*1);
    if(location.toString().match(/^https:\/\/www\.edominations\.com\/(?:$|\w{2}\/index)/)) {
        setTimeout(function(){
            $(document).off("click", '.add-as-friend');
            $('li.list-group-item').each(function(){
                $(this).find('.add-as-friend').attr('href', '#');
                if(t.data.blocks[$($(this).find('a.btn')[0]).attr('href').split('/').pop()]) $(this).remove();
            });
            $('.add-as-friend').click(function(event){
                event.preventDefault();
                var link = 'https://www.edominations.com/en/profile/'+$(this).attr('data-add');
                var formData = new FormData();
                formData.append("add", link.split('/').pop());
                var request = new XMLHttpRequest();
                request.open("POST", link);
                request.send(formData);
                $('a[data-add="'+link.split('/').pop()+'"]').remove();
            });
        }, 1e3);
    }
}
eDomPlus.prototype.load = function(){
    this.data=JSON.parse(localStorage.getItem("eDp"));
    if(!this.data) this.data = {};
    if(!this.data.alerts) this.data.alerts = [];
    if(!this.data.blocks) this.data.blocks = {};
    if(this.data.donations) {
        delete this.data.donations;
        delete this.data.settings.donationLog;
    }
    if(!this.data.globals) this.data.globals = {};
    if(!this.data.houses) this.data.houses = [];
    if(!this.data.offers) this.data.offers = {};
    if(!this.data.stats) this.data.stats = {};
    if(!this.data.settings) {
        this.data.settings = {};
        this.store();
        this.load();
    } else {
        if(!this.data.settings.alertHouses) this.data.settings.alertHouses = {label: 'Get alerts of houses near to expire', active: true};
        if(!this.data.settings.autofillMaxMoney) this.data.settings.autofillMaxMoney = {label: 'Autofill max in Monetary Market', active: true};
        if(!this.data.settings.battleFilter) this.data.settings.battleFilter = {label: 'Battle filter', active: true};
        if(!this.data.settings.battleInfo) this.data.settings.battleInfo = {label: 'Round overview on hover', active: true};
        if(!this.data.settings.dayStats) this.data.settings.dayStats = {label: 'Show the dialy DMG/HITS in sidebar', active: true};
        if(!this.data.settings.fastTravel) this.data.settings.fastTravel = {label: 'Add fast travel buttons', active: true};
        if(!this.data.settings.improveArticle) this.data.settings.improveArticle = {label: 'Improve article pages', active: true};
        if(!this.data.settings.improveBattlefield) this.data.settings.improveBattlefield = {label: 'Improve battle pages', active: true};
        if(!this.data.settings.improveCompanies) this.data.settings.improveCompanies = {label: 'Improve Company page', active: true};
        if(!this.data.settings.improveMarkets) this.data.settings.improveMarkets = {label: 'Improve Markets', active: true};
        if(!this.data.settings.improveFoodMkt) this.data.settings.improveFoodMkt = {label: 'Show CC/HP and highlight best food offer', active: true};
        if(!this.data.settings.improveStorage) this.data.settings.improveStorage = {label: 'Improve Storage', active: true};
        if(!this.data.settings.regionInfo) this.data.settings.regionInfo = {label: 'Show Defense Systems', active: true};
        if(!this.data.settings.removeBackground) this.data.settings.removeBackground = {label: 'Remove the water background', active: false};
        if(!this.data.settings.sellAlert) this.data.settings.sellAlert = {label: 'Alert when item is sold', active: true};
        if(!this.data.settings.showMuStats) this.data.settings.showMuStats = {label: 'Show MU members stats', active: true};
        if(!this.data.settings.showLocalCC) this.data.settings.showLocalCC = {label: 'Show Country CC in Monetary Market', active: true};
        if(!this.data.settings.showValue) this.data.settings.showValue = {label: 'Show total CC & Gold on sell offers', active: true};
        if(!this.data.settings.updateBtfOnHit) this.data.settings.updateBtfOnHit = {label: 'Update battle damages on each hit', active: false};
    }
    this.store();
};
eDomPlus.prototype.store = function(){
    localStorage.setItem("eDp", JSON.stringify(this.data));
};

eDomPlus.prototype.addAlert = function(info){
    var t = this;
    this.data.alerts.push({info: info});
    this.store();
};
eDomPlus.prototype.autofillMaxMoney = function(){
    var money, m;
    if(location.toString().match(/monetary-market\/\d+\/0/)) {
        money = parseFloat($($('.vs105')[1]).text().replace(" ",""));
        m = $($('.vs104')[2]).clone();
        $(m).find('.vs105').remove();
        m = $(m).text().trim();
    } else if(location.toString().match(/monetary-market\/\d+\/1/)) {
        money = parseFloat($($('.vs105')[0]).text().replace(" ",""));
        m = $($('.vs104')[1]).clone();
        $(m).find('.vs105').remove();
        m = $(m).text().trim();
    }
    $('.offerBuy').unbind();
    $('.offerBuy').keyup(function () {
        var offer = $(this).attr("id").split("_");
        var quantity = parseFloat($(".offerBuy_"+offer[1]).val());
        if(isNaN(quantity)) quantity = 0;
        var total;
        if(offer[2] >= quantity) {
            total = quantity * offer[3];
        } else if(offer[2] < quantity) {
            total = offer[2] * offer[3];
            $(this).val(offer[2]);
        }
        total = total.toFixed(3);
        $("#offerBuy_"+offer[1]).html(total+" "+m);
    });
    if($('.btn-group img').attr('src').split("/")[5] == $('.vs106').attr('src').split("/")[5]) {
        $('.offerBuy').each(function(){
            var row = $(this).parent().parent().parent();
            var amount = parseFloat($(this).attr('id').split("_")[2]);
            var rate = parseFloat($(this).attr('id').split("_")[3]);
            var maxmoney = money/rate;
            if(maxmoney > amount) maxmoney = amount;
            $(this).val((maxmoney).toFixed(3)).trigger('keyup');
        });
    }
};
eDomPlus.prototype.battleInfo = function(){
    var t = this;
    $('.vs151-7').hover(function(){
        var id = $(this).parent().children(".hasCountdown").attr("id").split("-")[1];
        if($(this).parent().children(".battle-info").length === 0) {
            var elem = this;
            $.ajax({url: 'https://www.edominations.com/en/battlefield/'+id+'/1', async: false}).done(function(data){
                var round = $("#panel-3 > div ", data).length;
                var tot_att_dmg = parseFloat($("#panel-1 div.vs917-17", data).text().replace(/,/g, ''));
                var tot_def_dmg = parseFloat($("#panel-1 div.vs917-18", data).text().replace(/,/g, ''));
                var att_dmg = parseFloat($("#panel-3 #round-"+round+" div.vs917-17",$(data)).text().replace(/,/g, ''));
                var def_dmg = parseFloat($("#panel-3 #round-"+round+" div.vs917-18",$(data)).text().replace(/,/g, ''));
                var tot = att_dmg+def_dmg;
                var att_pct = (att_dmg*100/tot).toFixed(2);
                var def_pct = (100-att_pct).toFixed(2);
                var att = $(elem).parent().find("span.vs151-6")[0];
                var def = $(elem).parent().find("span.vs151-6")[1];
                $(att).html('<sup>'+(isNaN(att_pct)?0:att_pct)+'% </sup><b>'+$('b',att).html()+'</b>');
                $(def).html('<b>'+$('b',def).html()+'</b><sup> '+(isNaN(def_pct)?0:def_pct)+'%</sup>');
                var html = "<div>";
                var att_dmgs = [];
                var def_dmgs = [];
                var row, nick, dmg;
                $(".vs917-4", $("#round-"+round, data).last()).each(function(){
                    if($(this, data).parents(".vs917-3").length){
                        row = $("> :nth-child(2n+3)",this);
                        nick = $(row[0]).text();
                        dmg = $(row[1]).text();
                        if(dmg!=="") att_dmgs.push({nick: nick, dmg: dmg});
                    } else if($(this, data).parents(".vs917-10").length){
                        row = $("> :nth-child(2n+1)",this);
                        nick = $(row[1]).text();
                        dmg = $(row[0]).text();
                        if(dmg!="...") def_dmgs.push({nick: nick, dmg: dmg});
                    }
                });
                html+="<table>";
                html+="<tr><td class=\"left\">"+addCommas(tot_att_dmg)+"</td><td colspan=\"2\">Total</td><td class=\"right\">"+addCommas(tot_def_dmg)+"</td></tr>";
                html+="<tr><td class=\"left\">"+addCommas(att_dmg)+"</td><td colspan=\"2\">Round "+round+"</td><td class=\"right\">"+addCommas(def_dmg)+"</td></tr>";
                html+="<tr><td>&nbsp;</td></tr>";
                for(var i=0; i<8;i++){
                    att = att_dmgs.shift();
                    def = def_dmgs.shift();
                    html+="<tr>";
                    html+="<td class=\"left\">"+(att!==undefined?att.nick:"")+"</td><td style=\"padding:0 0 0 10px;\" class=\"right\">"+(att!==undefined?att.dmg:"")+"</td>";
                    html+="<td style=\"padding: 0 10px 0 0;\" class=\"right\">"+(def!==undefined?def.dmg:"")+"</td><td class=\"right\">"+(def!==undefined?def.nick:"")+"</td>";
                    html+="</tr>";
                }
                html += "</table></div>";
                $(elem).tooltip({title: html, html: true, placement: "auto top"});
                $(elem).tooltip("show");
                $(elem).tooltip({trigger: "hover"});
            });
        }
    }, function(){});
};
eDomPlus.prototype.battleFilter = function(){
    var t = this;
    $(".panel-heading").append('<div id="eDp-filter" style="margin-top: -2px;float: right;"><div class="btn-group"><button type="button" class="btn btn-info" style="overflow: hidden;"><div id="filter-selected">All</div></button><button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></button><ul class="dropdown-menu vs524" role="menu" style="overflow-y: scroll;max-height: 243px;"><li><a id="all-war" class="war-filter vs116 vs520-s" href="javascript:;"><p style="margin: auto;"><b>All Wars</b></p></a></li><li><a id="resistance-war" class="war-filter vs116 vs520-s" href="javascript:;"><p style="margin: auto;"><b>Resistance</b></p></a></li><li><a id="r0-war" class="war-filter vs116 vs520-s" href="javascript:;"><p style="margin: auto;"><b>I.Urban</b></p></a></li><li><a id="r1-war" class="war-filter vs116 vs520-s" href="javascript:;"><p style="margin: auto;"><b>II.Land</b></p></a></li><li><a id="r2-war" class="war-filter vs116 vs520-s" href="javascript:;"><p style="margin: auto;"><b>III.Air</b></p></a></li></ul></div></div>');
    $(".vs593").each(function(){
        var tot;
        $("b", this).each(function(index){
            if(index === 0) tot = 0;
            tot+=parseFloat($(this).text());
        });
        $("sup", this).each(function(){
            $(this).remove();
        });
        $(".btn", this).after('<span style="float: right; margin-right: 15px; width: 15px;">'+edp.rounds[parseInt(tot/2)]+'</span>');
        $(this).addClass('r'+parseInt(tot/2)+'-war');
        if($(this).find(".vs151-5").length>0) $(this).addClass("resistance-war");
        else $(this).addClass("normal-war");
    });
    $(".war-filter").click(function(){
        var type = $(this).attr("id");
        if(type == "all-war"){
            changeCss(".normal-war", "display: block;");
            changeCss(".resistance-war", "display: block;");
            changeCss(".r0-war", "display: block;");
            changeCss(".r1-war", "display: block;");
            changeCss(".r2-war", "display: block;");
        } else {
            changeCss(".normal-war", "display: none;");
            changeCss(".resistance-war", "display: none;");
            changeCss(".r0-war", "display: none;");
            changeCss(".r1-war", "display: none;");
            changeCss(".r2-war", "display: none;");
            changeCss("."+type, "display: block!important;");
        }
        $("#filter-selected").text($(this).text());
    });
};
eDomPlus.prototype.calcDmg = function(){
    waitFor('#dmg-helper', function(elem){
        if($('#dmg-helper').attr('ready') == 3) {
            var bonus = 100.0;
            var ne = 1.0;
            var partisan = parseFloat($('#dmg-helper').attr('partisan'));
            var surround = parseInt($('#dmg-helper').attr('surround'));
            if($('#dmg-helper').attr('ne') == true) ne = 1.1;
            var ds = $('#dmg-helper').attr('ds');
            var lvl = parseInt($('#dmg-helper').attr('lvl'));
            var str = parseFloat($('#dmg-helper').attr('str'));
            var rnk = parseFloat($('#dmg-helper').attr('rnk'));
            var dmg = parseFloat(Math.ceil((lvl*5 + str) * (1 + rnk/20.0)) * ne * (100-partisan-ds)/100.0 * (100+surround)/100.0);
            $('#dmg-helper').attr('dmg', dmg);
            var round = $('#panel-3 > div').length;
            var weaps = [];
            $('.vs913-3').each(function(){
                var w = $('> a',this).attr('id').split('_')[1];
                if(w > 0 && w <= 12) {
                    var weapon;
                    if($('.vs914-11', this).attr('bonus')) weapon = parseFloat($('.vs914-11', this).attr('bonus'));
                    else weapon = parseFloat($('.vs914-11', this).text().replace(/%|x/, ''));
                    $('.vs914-11', this).attr('bonus', weapon);
                    var rounddmg = 1;
                    if(weapon != 1){
                        if(round == 1) {
                            rounddmg = 1.5;
                        } else if(round == 2) {
                            if(weapon <= 2) rounddmg = 0.8;
                            else rounddmg = 1.1;
                        } else {
                            if(weapon <= 2) rounddmg = 0.5;
                            else if(weapon <= 3) rounddmg = 0.9;
                        }
                    }
                    $('.vs914-11', this).text(addCommas(Math.round(dmg*rounddmg*weapon)));
                }
            });
        }
    });
};
eDomPlus.prototype.calcPrices = function(change){
    var t = this;
    var q = $("#sellQuality").attr("value");
    var item = $("#sellItem").attr("value");
    var country = $("#sellLic").attr("value");
    var price = $('#sellPrice').attr("value");
    var sellAmount = jQuery("#sellAmount").val();
    var sellTax = 0;
    if(tax_cc==country) {
        sellTax = tax[country].TV[item];
    } else {
        sellTax = tax[country].TI[item];
    }
    var sellTotal = sellAmount * price / 100 * sellTax;
    sellTotal = sellTotal.toFixed(3);
    jQuery("#sellCC").html("("+sellTax+"%) "+sellTotal+" "+tax[country].CC);
    var value = $("#sellAmount").val()*$("#sellPrice").val();
    value = (value.toFixed(3)-sellTotal).toFixed(3);
    $("#sellTot").text(" Total "+value+" "+tax[country].CC);
    var gold = (value*change).toFixed(3);
    $("#sellGold").text(" Gold "+gold+"");
};
eDomPlus.prototype.checkHouses = function(){
    var t = this;
    if(!t.data.last_hcheck || t.data.last_hcheck < this.ed_day - 1){
        t.updateHouses(function (){
            for(var house in t.data.houses){
                var q = house;
                house = t.data.houses[house];
                if(house !== null && house.active && !house.alerted && house.days <= 2) {
                    var end = parseInt(house.days)+parseInt(t.ed_day);
                    if(house.stock>0) {
                        t.addAlert("Your house Q"+q+' expires on day '+end+' <a style="margin-left: 5px;" value="'+q+'" class="btn btn-info btn-3d vs535 activate-house">Activate</a>');
                    } else {
                        t.addAlert("Your house Q"+q+' expires on day '+end);
                    }
                    house.alerted=true;
                }
            }
        });
    }
    this.store();
};
eDomPlus.prototype.checkVersion = function(){
    var t = this;
    if(!this.data.last_vcheck || this.data.last_vcheck < this.ed_day - 1){
        $.ajax({url: "https://bitbucket.org/Rubensei/edom-plus/raw/"+t.branch+"/edom-plus.meta.js", cache: false, async: false}).done(function(data){
            t.data.version = parseFloat(data.match(/@version[^\d]*(.*)/)[1]);
            t.data.last_vcheck = t.ed_day;
            t.store();
        });
    }
    if(this.data.version > GM_info.script.version.replace("r", "")){
        $($(".side-nav-info")[0]).append('<a href="https://bitbucket.org/Rubensei/edom-plus/raw/'+t.branch+'/edom-plus.user.js" style="padding: 0px; border: 0px; background-color: #5ab25a;"><div class="vs104" style="width: 100%;"><center><b>Update '+this.data.version+'</b></center></div></a>');
    } else {
        $($(".side-nav-info")[0]).append('<div class="vs104" style="background-color: #3a9ed3;" style="width: 100%;"><center><b>eDom Plus '+GM_info.script.version+(t.branch == 'beta' ? 'b' : '')+'</b> <a style="padding: 0px; border: 0px; background-color: transparent;" id="check-version" href="javascript:;"><i style="margin: 0; opacity: 1;" class="fa fa-refresh vs536"></i></a> <a style="padding: 0px; border: 0px; background-color: transparent;" id="edp-settings" href="javascript:;"><i style="margin: 0; opacity: 1;" class="fa fa-gear vs536""></i></a></center></div>');
        $("#check-version").tooltip({title: "Click to Check for Updates", placement: "top"});
        $("#edp-settings").tooltip({title: "Open eDp settings", placement: "top"});
    }
    $("#check-version").click(function(){
        t.load();
        delete t.data.alerts;
        delete t.data.houses;
        delete t.data.last_vcheck;
        delete t.data.last_hcheck;
        t.store();
        location.reload();
    });
    $('body').append('<div id="edp-settings-back" style="display: none;position: fixed;z-index: 999998;top: 0;width: 100%;height: 100%;background: rgba(0,0,0,.6);"></div>');
    $('body').append('<div id="edp-settings-div" style="display: none;position: fixed;background: #000;box-shadow: 0 1px 4px;cursor: default;top: 50%;left: 50%;transform: translate(-50%,-50%);z-index: 999999;border-radius: 5px;"><div style="margin: 5px; padding: 2px; color: white;"><ul style="list-style-type: none; padding: 0; margin-bottom: -5px;"></ul></div></div>');
    for(var setting in this.data.settings) {
        var name = setting;
        setting = this.data.settings[setting];
        $('#edp-settings-div div ul').append('<li><label class="script-setting" style="display: block; padding-left: 15px; text-indent: -15px;"><input type="checkbox" name="settings" value="'+name+'" '+(setting.active?'checked':'')+' style="padding: 0; margin:0; vertical-align: bottom; position: relative; top: -2px; margin-right: 3px; *overflow: hidden;">'+setting.label+'<label></li>');
    }
    $('#edp-settings').click(function(){
        $('#edp-settings-div, #edp-settings-back').css("display","block");
    });
    $('#edp-settings-back').click(function(){
        $('#edp-settings-div, #edp-settings-back').css("display","none");
    });
    $('.script-setting input').click(function(){
        var checked = $(this).attr('checked');
        $(this).attr('checked', !$(this).attr('checked'));
        var name = $(this).attr('value');
        t.data.settings[name].active = $(this).attr('checked');
        t.store();
    });
};
eDomPlus.prototype.dayStats = function(){
    var t = this;
    if(location.toString().match(/battlefield/)) {
        $.ajaxSetup({
            complete: function(xhr, textStatus) {
                var data = xhr.responseText;
                if(typeof data === 'string' && data.isJSON())
                    data = JSON.parse(data);
                if(data) data = data[0];
                if(data && data.HTML_LOG){
                    var r = data.HTML_LOG;
                    var regex = /<span class="vs916-6 badge vs916-9">\+([\d,]+)<\/span>/g;
                    var match = regex.exec(r);
                    var i = 0;
                    while (match !== null) {
                        if(i === 0) {
                            t.data.stats.dmg += parseInt(match[1].replace(",", ""));
                        } else if(i == 2){
                            t.data.stats.hits += parseInt(match[1]);
                        }
                        i++;
                        match = regex.exec(r);
                    }
                    t.store();
                }
            }
        });
    }
    $($('.side-nav-info')[0]).append('<div class="vs104">Damage <b style="float: right;">'+addCommas(t.data.stats.dmg)+'</b><br>Hits <b style="float: right;">'+t.data.stats.hits+'</b></div>');
};
eDomPlus.prototype.fastTravel = function(){
    var t = this;
    if($('.vs915').length > 0){
        var travel = $('.vs915').find('a')[0];
        var link = $(travel).addClass('travel').attr('value','trevel-energy').text("Change Location (Energy)").attr("href");
        $(travel).after('<a class="btn btn-info btn-bordered vs915-2 travel" value="trevel" href="#" style="margin-left: 5px;">Change Location (Gold)</a>');
        $(travel).attr("href","#");
        $(".travel").click(function(){
            var id = link.split("/")[8];
            var formData = new FormData();
            formData.append($(this).attr("value"), id);
            var request = new XMLHttpRequest();
            request.open("POST", link);
            request.onreadystatechange = function(){
                location.reload();
            };
            request.send(formData);
        });
    }
};
eDomPlus.prototype.getCCExchange = function(f, country){
    var t = this;
    if(country === undefined) country = $(".side-nav-info .vs107").attr("href").split("/")[6];
    $.ajax({url: "https://www.edominations.com/en/monetary-market/"+country+"/1/1"}).done(function(data){
        if($("#panel-3 .vsTable tbody tr", data).length > 0){
            var change = -1;
            for(var i = 0; i < $("#panel-3 .vsTable tbody tr", data).length && change < 0; i++){
                var row = $("#panel-3 .vsTable tbody tr", data)[i];
                if(parseFloat($(row).find('td:eq(2) strong').text().replace(' ', ''))>250.0){
                    change = parseFloat($(row).find('td:eq(3) strong:eq(1)').text());
                }
            }
            if(change == -1) change = null;
            f(change);
        } else {
            f(null);
        }
    });
};
eDomPlus.prototype.globalCompare = function(){
    var t = this;
    var o_html = '<div class="col-xs-12 .col-sm-12 .col-md-12"><div class="panel" style="min-height: 1200px;"><script src="/public/js/numberChecks.js" type="text/javascript"></script><script src="/public/js/findCountries.js" type="text/javascript"></script><div class="panel panel-theme vs504 vs512"><div class="panel-heading vs500"><div class="panel-title"><strong class="vs501">Global Price Comparison (It\'s slow, please let it finish)</strong></div></div><div class="panel-body vs502 panel-full"><div class="row" style="margin-bottom: 10px;"><div class="dropdown vs532" data-toggle="tooltip" data-html="true" data-placement="top" title="" data-original-title="Food Mat."><a href="https://www.edominations.com/en/market-compare/1/1"><button type="button" class="btn btn-theme vs534 dropdown-toggle" style="width: 84px;"><img src="/public/game/items/food-raw.png" width="50" height="50" alt=""></button></a></div><div class="dropdown vs532" data-toggle="tooltip" data-html="true" data-placement="top" title="" data-original-title="Weapon Mat."><a href="https://www.edominations.com/en/market-compare/2/1"><button type="button" class="btn btn-theme vs534 dropdown-toggle" style="width: 84px;"><img src="/public/game/items/weapon-raw.png" width="50" height="50" alt=""></button></a></div><div class="dropdown vs532" data-toggle="tooltip" data-html="true" data-placement="top" title="" data-original-title="Building Mat."><a href="https://www.edominations.com/en/market-compare/3/1"><button type="button" class="btn btn-theme vs534 dropdown-toggle" style="width: 84px;"><img src="/public/game/items/building-raw.png" width="50" height="50" alt=""></button></a></div><div class="dropdown vs532" data-toggle="tooltip" data-html="true" data-placement="top" title="" data-original-title="Food"><button type="button" class="btn btn-theme vs534 dropdown-toggle" data-toggle="dropdown"><img src="/public/game/items/food.png" width="50" height="50" alt=""><span class="caret"></span></button><ul class="dropdown-menu vs524"><li><a class="vs116" href="https://www.edominations.com/en/market-compare/4/0"><img class="vs529" src="/public/game/items/food.png"><div class="vs108 vs533"><i class="star star-0"></i></div></a></li><li><a class="vs116" href="https://www.edominations.com/en/market-compare/4/1"><img class="vs529" src="/public/game/items/food.png"><div class="vs108 vs533"><i class="star star-1"></i></div></a></li><li><a class="vs116" href="https://www.edominations.com/en/market-compare/4/2"><img class="vs529" src="/public/game/items/food.png"><div class="vs108 vs533"><i class="star star-2"></i></div></a></li><li><a class="vs116" href="https://www.edominations.com/en/market-compare/4/3"><img class="vs529" src="/public/game/items/food.png"><div class="vs108 vs533"><i class="star star-3"></i></div></a></li><li><a class="vs116" href="https://www.edominations.com/en/market-compare/4/4"><img class="vs529" src="/public/game/items/food.png"><div class="vs108 vs533"><i class="star star-4"></i></div></a></li><li><a class="vs116" href="https://www.edominations.com/en/market-compare/4/5"><img class="vs529" src="/public/game/items/food.png"><div class="vs108 vs533"><i class="star star-5"></i></div></a></li></ul></div><div class="dropdown vs532" data-toggle="tooltip" data-html="true" data-placement="top" title="" data-original-title="Weapons"><button type="button" class="btn btn-theme vs534 dropdown-toggle" data-toggle="dropdown"><img src="/public/game/items/weapons.png" width="50" height="50" alt=""><span class="caret"></span></button><ul class="dropdown-menu vs524"><li><a class="vs116" href="https://www.edominations.com/en/market-compare/5/0"><img class="vs529" src="/public/game/items/weapons.png"><div class="vs108 vs533"><i class="star star-0"></i></div></a></li><li><a class="vs116" href="https://www.edominations.com/en/market-compare/5/1"><img class="vs529" src="/public/game/items/weapons.png"><div class="vs108 vs533"><i class="star star-1"></i></div></a></li><li><a class="vs116" href="https://www.edominations.com/en/market-compare/5/2"><img class="vs529" src="/public/game/items/weapons.png"><div class="vs108 vs533"><i class="star star-2"></i></div></a></li><li><a class="vs116" href="https://www.edominations.com/en/market-compare/5/3"><img class="vs529" src="/public/game/items/weapons.png"><div class="vs108 vs533"><i class="star star-3"></i></div></a></li><li><a class="vs116" href="https://www.edominations.com/en/market-compare/5/4"><img class="vs529" src="/public/game/items/weapons.png"><div class="vs108 vs533"><i class="star star-4"></i></div></a></li><li><a class="vs116" href="https://www.edominations.com/en/market-compare/5/5"><img class="vs529" src="/public/game/items/weapons.png"><div class="vs108 vs533"><i class="star star-5"></i></div></a></li></ul></div><div class="dropdown vs532" data-toggle="tooltip" data-html="true" data-placement="top" title="" data-original-title="Tank"><button type="button" class="btn btn-theme vs534 dropdown-toggle" data-toggle="dropdown"><img src="/public/game/items/tank.png" width="50" height="50" alt=""><span class="caret"></span></button><ul class="dropdown-menu vs524"><li><a class="vs116" href="https://www.edominations.com/en/market-compare/6/0"><img class="vs529" src="/public/game/items/tank.png"><div class="vs108 vs533"><i class="star star-0"></i></div></a></li><li><a class="vs116" href="https://www.edominations.com/en/market-compare/6/1"><img class="vs529" src="/public/game/items/tank.png"><div class="vs108 vs533"><i class="star star-1"></i></div></a></li><li><a class="vs116" href="https://www.edominations.com/en/market-compare/6/2"><img class="vs529" src="/public/game/items/tank.png"><div class="vs108 vs533"><i class="star star-2"></i></div></a></li><li><a class="vs116" href="https://www.edominations.com/en/market-compare/6/3"><img class="vs529" src="/public/game/items/tank.png"><div class="vs108 vs533"><i class="star star-3"></i></div></a></li><li><a class="vs116" href="https://www.edominations.com/en/market-compare/6/4"><img class="vs529" src="/public/game/items/tank.png"><div class="vs108 vs533"><i class="star star-4"></i></div></a></li><li><a class="vs116" href="https://www.edominations.com/en/market-compare/6/5"><img class="vs529" src="/public/game/items/tank.png"><div class="vs108 vs533"><i class="star star-5"></i></div></a></li></ul></div><div class="dropdown vs532" data-toggle="tooltip" data-html="true" data-placement="top" title="" data-original-title="Aircraft"><button type="button" class="btn btn-theme vs534 dropdown-toggle" data-toggle="dropdown"><img src="/public/game/items/aircraft.png" width="50" height="50" alt=""><span class="caret"></span></button><ul class="dropdown-menu vs524"><li><a class="vs116" href="https://www.edominations.com/en/market-compare/7/0"><img class="vs529" src="/public/game/items/aircraft.png"><div class="vs108 vs533"><i class="star star-0"></i></div></a></li><li><a class="vs116" href="https://www.edominations.com/en/market-compare/7/1"><img class="vs529" src="/public/game/items/aircraft.png"><div class="vs108 vs533"><i class="star star-1"></i></div></a></li><li><a class="vs116" href="https://www.edominations.com/en/market-compare/7/2"><img class="vs529" src="/public/game/items/aircraft.png"><div class="vs108 vs533"><i class="star star-2"></i></div></a></li><li><a class="vs116" href="https://www.edominations.com/en/market-compare/7/3"><img class="vs529" src="/public/game/items/aircraft.png"><div class="vs108 vs533"><i class="star star-3"></i></div></a></li><li><a class="vs116" href="https://www.edominations.com/en/market-compare/7/4"><img class="vs529" src="/public/game/items/aircraft.png"><div class="vs108 vs533"><i class="star star-4"></i></div></a></li><li><a class="vs116" href="https://www.edominations.com/en/market-compare/7/5"><img class="vs529" src="/public/game/items/aircraft.png"><div class="vs108 vs533"><i class="star star-5"></i></div></a></li></ul></div><div class="dropdown vs532" data-toggle="tooltip" data-html="true" data-placement="top" title="" data-original-title="House"><button type="button" class="btn btn-theme vs534 dropdown-toggle" data-toggle="dropdown"><img src="/public/game/items/house.png" width="50" height="50" alt=""><span class="caret"></span></button><ul class="dropdown-menu vs524"><li><a class="vs116" href="https://www.edominations.com/en/market-compare/8/0"><img class="vs529" src="/public/game/items/house.png"><div class="vs108 vs533"><i class="star star-0"></i></div></a></li><li><a class="vs116" href="https://www.edominations.com/en/market-compare/8/1"><img class="vs529" src="/public/game/items/house.png"><div class="vs108 vs533"><i class="star star-1"></i></div></a></li><li><a class="vs116" href="https://www.edominations.com/en/market-compare/8/2"><img class="vs529" src="/public/game/items/house.png"><div class="vs108 vs533"><i class="star star-2"></i></div></a></li><li><a class="vs116" href="https://www.edominations.com/en/market-compare/8/3"><img class="vs529" src="/public/game/items/house.png"><div class="vs108 vs533"><i class="star star-3"></i></div></a></li><li><a class="vs116" href="https://www.edominations.com/en/market-compare/8/4"><img class="vs529" src="/public/game/items/house.png"><div class="vs108 vs533"><i class="star star-4"></i></div></a></li><li><a class="vs116" href="https://www.edominations.com/en/market-compare/8/5"><img class="vs529" src="/public/game/items/house.png"><div class="vs108 vs533"><i class="star star-5"></i></div></a></li></ul></div><div class="dropdown vs532" data-toggle="tooltip" data-html="true" data-placement="top" title="" data-original-title="Hospital"><button type="button" class="btn btn-theme vs534 dropdown-toggle" data-toggle="dropdown"><img src="/public/game/items/hospital.png" width="50" height="50" alt=""><span class="caret"></span></button><ul class="dropdown-menu vs524"><li><a class="vs116" href="https://www.edominations.com/en/market-compare/9/0"><img class="vs529" src="/public/game/items/hospital.png"><div class="vs108 vs533"><i class="star star-0"></i></div></a></li><li><a class="vs116" href="https://www.edominations.com/en/market-compare/9/1"><img class="vs529" src="/public/game/items/hospital.png"><div class="vs108 vs533"><i class="star star-1"></i></div></a></li><li><a class="vs116" href="https://www.edominations.com/en/market-compare/9/2"><img class="vs529" src="/public/game/items/hospital.png"><div class="vs108 vs533"><i class="star star-2"></i></div></a></li><li><a class="vs116" href="https://www.edominations.com/en/market-compare/9/3"><img class="vs529" src="/public/game/items/hospital.png"><div class="vs108 vs533"><i class="star star-3"></i></div></a></li><li><a class="vs116" href="https://www.edominations.com/en/market-compare/9/4"><img class="vs529" src="/public/game/items/hospital.png"><div class="vs108 vs533"><i class="star star-4"></i></div></a></li><li><a class="vs116" href="https://www.edominations.com/en/market-compare/9/5"><img class="vs529" src="/public/game/items/hospital.png"><div class="vs108 vs533"><i class="star star-5"></i></div></a></li></ul></div><div class="dropdown vs532" data-toggle="tooltip" data-html="true" data-placement="top" title="" data-original-title="Defense System"><button type="button" class="btn btn-theme vs534 dropdown-toggle" data-toggle="dropdown"><img src="/public/game/items/defense-system.png" width="50" height="50" alt=""><span class="caret"></span></button><ul class="dropdown-menu vs524"><li><a class="vs116" href="https://www.edominations.com/en/market-compare/10/0"><img class="vs529" src="/public/game/items/defense-system.png"><div class="vs108 vs533"><i class="star star-0"></i></div></a></li><li><a class="vs116" href="https://www.edominations.com/en/market-compare/10/1"><img class="vs529" src="/public/game/items/defense-system.png"><div class="vs108 vs533"><i class="star star-1"></i></div></a></li><li><a class="vs116" href="https://www.edominations.com/en/market-compare/10/2"><img class="vs529" src="/public/game/items/defense-system.png"><div class="vs108 vs533"><i class="star star-2"></i></div></a></li><li><a class="vs116" href="https://www.edominations.com/en/market-compare/10/3"><img class="vs529" src="/public/game/items/defense-system.png"><div class="vs108 vs533"><i class="star star-3"></i></div></a></li><li><a class="vs116" href="https://www.edominations.com/en/market-compare/10/4"><img class="vs529" src="/public/game/items/defense-system.png"><div class="vs108 vs533"><i class="star star-4"></i></div></a></li><li><a class="vs116" href="https://www.edominations.com/en/market-compare/10/5"><img class="vs529" src="/public/game/items/defense-system.png"><div class="vs108 vs533"><i class="star star-5"></i></div></a></li></ul></div></div></div></div></div></div>';
    $('.row.vs222').html(o_html);
    $('body').append('<div id="countries-ready" ready="0"></div>');
    var url = location.toString().split('/');
    var q = url.pop();
    var item = url.pop();
    if(item !== '0' && q !== '0') {
        $('.panel-full').append('<div class="table-responsive vsTable" style="overflow-x: unset;"><table class="table"><thead><tr><th width="50"><span>Country</span></th><th><span>Seller</span></th><th width="150"><span>Supply</span></th><th width="150"><span>Price</span></th><th width="300"><span>Link to Market</span></th></tr></thead><tbody></tbody></table></div>');
        $('.vsTable').before('<div id="progress"><h3>Progress</h3><div class="progress" id="progress" style="margin: 15px 10px;"><div class="progress-bar" id="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:0%"></div></div></div>');
        var litem;
        for(var j in t.items) {
            if(t.items[j].id == parseInt(item)) litem = j;
        }
        for(var i = 1; i <= 78; i++){
            (function(index){
                $('body').append('<div class="ready" id="ready-'+index+'"></div>');
                $('#ready-'+index).attr('ready', 'false');
                $('#ready-'+index).click(function(){
                    var cid = $(this).attr('id').split('-')[1];
                    var change = parseFloat($(this).attr('ready'));
                    var row = $('#country-'+cid);
                    var price = parseFloat($('td:nth-child(4) strong', row).text().replace(' ', ''));
                    var label = 'Gold / Unit';
                    if(item < '8') {
                        price *= 100;
                        label = 'Gold / 100 Units';
                    }
                    if(change !== Infinity) $('td:nth-child(4)', row).append('<br><strong>'+(price*change).toFixed(3)+'</strong> '+label);
                    $(row).attr('value', (price*change).toFixed(3));
                    var ready = parseInt($('#countries-ready').attr('ready'));
                    $('#countries-ready').attr('ready', ready+=1);
                    var width = ((parseInt(ready))*100/(78)).toFixed(2);
                    $("#progress-bar").css("width", width+"%");
                    if(ready == 78) {
                        $('.vsTable tbody').find('tr').sort(function (a, b) {
                            return parseFloat($(a).attr('value')) - parseFloat($(b).attr('value'));
                        }).appendTo($('.vsTable tbody'));
                    }
                });
                var url = 'https://www.edominations.com/en/market/'+index+'/'+item+'/'+q+'/1';
                $.ajax(url).done(function(data){
                    if($('.vsTable tbody tr', data).length > 0) {
                        var offer = $('.vsTable tbody tr:eq(0)', data).attr('id', 'country-'+index);
                        $('.vsTable tbody').append(offer);
                        var country = getKey(t.countries, index.toString());
                        $('.vsTable tbody tr[id="country-'+index+'"] td:last').html('<a href="https://www.edominations.com/en/market/'+index+'/'+item+'/'+q+'/1"><button class="btn btn-info btn-3d vs535">Go to Market Offer</button></a>');
                        $('.vsTable tbody tr[id="country-'+index+'"] td:first').html('<a href="https://www.edominations.com/en/country/society/'+index+'"><img src="/public/game/flags/flat/32/'+country+'.png" width="50" height="50" alt="'+country+'"></a>');
                        $('.vsTable tbody tr[id="country-'+index+'"] td:eq(2)').css('position', 'relative').append('<img style="position: absolute; right: 10px; top: 6px;" src="/public/game/items/'+litem+'.png" alt="" height="50">');
                    }
                    if($('#ready-'+index).attr('ready') == 'false') {
                        $('#ready-'+index).attr('ready', 'true');
                    } else {
                        $('#ready-'+index).click();
                    }
                });
                t.getCCExchange(function(change){
                    if(change === null) change = Infinity;
                    if($('#ready-'+index).attr('ready') == 'false') {
                        $('#ready-'+index).attr('ready', change);
                    } else {
                        $('#ready-'+index).attr('ready', change).click();
                    }
                }, index);
            })(i);
        }
    }
};
eDomPlus.prototype.improveArticle = function(){
    var t = this;
    $('.vs502 h4').append('<div id="randomEndorser" style="float: right;margin-right: 3px;"><ul style="list-style: none;"><li><button style="width: 158px;" class="btn btn-s btn-bordered btn-info">Pick Random Endorser</button><ul style="position: absolute; list-style: none; display: none; padding: 0px; background-color: white; z-index: 1;"><li value="25"><button style="width: 158px;" class="btn btn-s btn-bordered btn-info">25cc or more</button></li><li value="10"><button style="width: 158px;" class="btn btn-s btn-bordered btn-info">10cc or more</button></li><li value="5"><button style="width: 158px;" class="btn btn-s btn-bordered btn-info">5cc or more</button></li></ul></li></ul></div>');
    $('#randomEndorser').hover(function(){
        $("ul li ul",this).fadeIn(200);
    }, function(){
        $("ul li ul",this).fadeOut(200);
    });
    $('#randomEndorser ul li ul li').click(function(){
        var cc = $(this).val();
        var selector = 'a.vs118';
        if(cc <= 10) selector += ', a.vs119';
        if(cc == 5) selector += ', a.vs120';
        var winner = $(selector)[Math.floor(Math.random()*$(selector).length)];
        if(winner !== undefined) {
            winner = $(winner).css({'float': 'right', 'margin-top': '-2px', 'margin-left': '3px'});
            winner = $(winner).prop('outerHTML');
        } else winner = '<a href="javascript:;">you don\'t even have that kind of endorses :facepalm:</a>';
        if($('#winner').length === 0){
            $('.vs192-1').append('<div id="winner" style="position: absolute; right: 18px; z-index: 0;">The Chosen is: '+winner+'</div>');
        } else {
            $('#winner a').remove();
            $('#winner').append(winner);
        }
    });
};
eDomPlus.prototype.improveBattlefield = function(){
    var t = this;
    var i = 0;
    $('#battleVal_A').bind("DOMSubtreeModified",function(){setTimeout(function(){if((i = (i+=1)%2) === 0) t.updateBattlefield();},1000);});
    $('#panel-1').prepend(t.update_button);
    $('#panel-2').prepend(t.update_button);
    $('#panel-3').prepend(t.update_button);
    $('.updateBtf').click(function(){t.updateBattlefield();});
    var reg_id = $('.vs907-2 a').attr('href').split('/')[6];
    $.ajax('https://www.edominations.com/en/api2/travel/'+reg_id).done(function(data){
        data = JSON.parse(data);
        var ds = data[0][$('.vs907-2 a').attr('href').split('/')[8]].Defense;
        if(ds > 0) {
            $('.hasCountdown').after('<div><img src="https://www.edominations.com/public/game/items/defense-system.png" height="50" width="50"><br><span style="margin-left: 5px;">DS Q'+ds+'</span></div>');
        }
    });
    // Calc DMG
    var att = {}, def = {}, ctr = {}, side;
    $.ajax('https://www.edominations.com/en/profile/'+t.id).done(function(data){
        $('body').append('<div id="dmg-helper" style="display: none;"></div>');
        var ctr_link = $($('.vs156', data)[1]).find('a').attr('href');
        ctr.id = ctr_link.split('/')[ctr_link.split('/').length-2];
        ctr.s = ctr_link.split('/')[ctr_link.split('/').length-1];
        //Partisan & NE
        $.ajax('https://www.edominations.com/en/country/military/'+ctr.id+'/'+ctr.s).done(function(data){
            var ne = $('div.table-responsive:nth-child(3) > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(1) > td:nth-child(2) > a:nth-child(2)', data);
            ne = $(ne).attr('href').match(/\d+/)[0];
            $('#dmg-helper').attr('ne', $('#dmg-helper').attr('enemy') == ne);
            if($('#dmg-helper').attr('enemy') == ne) $('.vs908-4').append('<p>Natural Enemy 10%</p>')
            var partisan = parseFloat($('.table-responsive tbody tr:nth-child(1) td:nth-child(2) b:nth-child(3)', data).text().replace('%', ''));
            $('.vs908-4').append('<p>Partisan -'+partisan+'%</p>')
            $('#dmg-helper').attr('partisan', partisan);
            if($('#dmg-helper').attr('ready') == 3) t.calcDmg();
            else $('#dmg-helper').attr('ready', parseInt($('#dmg-helper').attr('ready'))+1);
        });
        var lvl = $('.vs164-6.vs164-13', data).text();
        var str = $('.vs164-2', data).text().replace(',', '').replace('.', ',');
        var rnk = $('.vs164-5', data).attr('src').match(/\d+/)[0];
        $('#dmg-helper').attr('lvl', lvl).attr('str', str).attr('rnk', rnk).attr('cs-id', ctr.id).attr('cs-s', ctr.s).attr('ready', 0).attr('bonus-tot', 0);
    });
    var reg_id = $('.vs907-2 a').attr('href').match(/\d+/g)[1];
    var att_link = $('.vs902-1 a').attr('href');
    att.id = att_link.split('/')[att_link.split('/').length-2];
    att.s = att_link.split('/')[att_link.split('/').length-1];
    var def_link = $('.vs905-1 a').attr('href');
    def.id = def_link.split('/')[def_link.split('/').length-2];
    def.s = def_link.split('/')[def_link.split('/').length-1];
    // DS
    $.ajax('https://www.edominations.com/en/api2/travel/'+def.id).done(function(data){
        data = JSON.parse(data)[0][reg_id];
        var ds = data.Defense;
        var dss = [0,3,5,7,11,15];
        waitFor('#dmg-helper', function(elem){
            $('#dmg-helper').attr('ds', ($('#dmg-helper').attr('side') == att.id ? dss[ds] : 0));
            if($('#dmg-helper').attr('ready') == 3) t.calcDmg();
            else $('#dmg-helper').attr('ready', parseInt($('#dmg-helper').attr('ready'))+1);
        });
    });
    //Surround
    $.ajax('https://www.edominations.com/en/api2/map/0').done(function(data){
        data = JSON.parse(data)[0];
        var surround = 0;
        var neighbours = data[reg_id].Neighbours;
        waitFor('#dmg-helper', function(elem){
            if($('#dmg-helper').attr('side') == att.id && att.id == $('#dmg-helper').attr('cs-id')){
                for(var i = 0; i < neighbours.length; i++){
                    var n = neighbours[i];
                    if(data[n].OwnerCurrentID == $('#dmg-helper').attr('side')) surround += 5;
                }
                $('#dmg-helper').attr('surround', surround);
                $('.vs908-4').append('<p>Surrounding +'+surround+'%</p>');
            } else {
                $('#dmg-helper').attr('surround', 0);
            }
            if($('#dmg-helper').attr('ready') == 3) t.calcDmg();
            else $('#dmg-helper').attr('ready', parseInt($(elem).attr('ready'))+1);
        });
    });
    waitFor('#battleFight', function(elem){
        var side = $(elem).hasClass('vs912-6-v1');
        side = (side ? att : def)
        waitFor('#dmg-helper', function(elem){
            $('#dmg-helper').attr('side', (side ? att.id : def.id)).attr('enemy', (side ? def.id : att.id));
            if($(elem).attr('ready') == 2) t.calcDmg();
            else $(elem).attr('ready', parseInt($(elem).attr('ready'))+1);
        });
    });
    var observer = new MutationObserver(function(mutations, observer) {
        mutations.forEach ( function (mutation) {
            if (typeof mutation.addedNodes == "object") {
                var n = $(mutation.addedNodes);
                if(n.is('div.vs912')) {
                    t.calcDmg();
                }
            }
        } );
    });
    $('#loadItems').each(function(){
        observer.observe(this,{childList: true, characterData: true, attributes: true, subtree: true});
    });
};
eDomPlus.prototype.improveCompanies = function(){
    var t = this;
    var food = 0,weap = 0,build = 0;
    $.ajax({url: "https://www.edominations.com/en/inventory"}).done(function(data){
        if($('.ng-scope img[src="/public/game/items/food-raw.png"].vs528-1', data).length>0){
            food = $('.ng-scope img[src="/public/game/items/food-raw.png"].vs528-1', data).parent().parent().find("strong.vs528-4").text();
        }
        if($('.ng-scope img[src="/public/game/items/weapon-raw.png"].vs528-1', data).length>0){
            weap = $('.ng-scope img[src="/public/game/items/weapon-raw.png"].vs528-1', data).parent().parent().find("strong.vs528-4").text();
        }
        if($('.ng-scope img[src="/public/game/items/building-raw.png"].vs528-1', data).length>0){
            build = $('.ng-scope img[src="/public/game/items/building-raw.png"].vs528-1', data).parent().parent().find("strong.vs528-4").text();
        }
        $("#stickerScroll td:last").before('<td class="text-center vs188"><span class="vs526">In Storage</span><span class="vs187"><img src="/public/game/items/food-raw.png" width="32" height="32" alt=""> '+food+'<br><img src="/public/game/items/weapon-raw.png" width="32" height="32" alt=""> '+weap+'<br><img src="/public/game/items/building-raw.png" width="32" height="32" alt=""> '+build+'</span></td>');
    });
};
eDomPlus.prototype.improveFoodMkt = function(){
    var t = this;
    var i = 0;
    var best;
    $(".table-responsive tbody tr").each(function(index){
        var q = parseFloat($($("i", this)[0]).attr("class").split("-")[1]);
        var p = $($("td", this)[3]).text().split(" ");
        var m = p.pop();
        p = parseFloat(p.join(""));
        if(!best) best=p/(q*2);
        if(p/(q*2)<=best){
            best = p/(q*2);
            i=index;
        }
        p = (p/(q*2)).toFixed(4).toString();
        if(p.indexOf(".")>-1) p=p.replace(/[0]+$/, "").replace(/[.]$/, "");
        $($("td", this)[3]).append('</br><sub>'+p+' '+m+'/HP'+'</sub>');
    });
    if(best) {
        best = $($(".table-responsive tbody tr")[i]);
        var b = $("button", $("td", best)[4]);
        b.attr("class", b.attr("class").replace("info","success"));
        $("span",b).text("Best Deal");
    }
};
eDomPlus.prototype.improveMarkets = function(){
    var t = this;
    var country_s = $('div.btn-group .dropdown-toggle img').attr('src').replace('.png', '').split('/')[6];
    $.ajax('https://www.edominations.com/en/profile/'+t.id).done(function(data){
        var country = t.countries[country_s];
        var cc = parseFloat($($('#tab-5 img[src="/public/game/flags/flat/32/'+country_s+'.png"]', data).parent().find('.vs588')[0]).text().replace(' ',''));
        $('.buyAmount').each(function(){
            var offer = $(this).parent().parent().parent();
            var price = parseFloat($(offer).find('td:eq(3) strong').text().replace(' ',''));
            var max = Math.floor(cc/price);
            var maxitems = parseFloat($(offer).find('td:eq(2)').text().replace(" ",""));
            if(max > maxitems) max = maxitems;
            $(offer).find('input.buyAmount').attr('value', max).trigger('keyup');
        });
        t.getCCExchange(function(change){
            change = parseFloat(change);
            if(!isNaN(change)) {
                $('tbody .star:odd').each(function(){
                    var row = $(this).parent().parent().parent();
                    var price = parseFloat($('td:nth-child(4) strong', row).text().replace(' ', ''));
                    var label = 'Gold / Unit';
                    if(parseInt(location.toString().split('/')[6]) < 8) {
                        price *= 100;
                        label = 'Gold / 100 Units';
                    }
                    $('td:nth-child(4)', row).append('<br><strong>'+(price*change).toFixed(3)+'</strong> '+label);
                });
            }
            $('.buyAmount').unbind();
            $('.buyAmount').keyup(function(){
                var total;
                var market_offer = $(this).attr("id").split("_");
                var market_quantity = parseInt($(".buyAmount_"+market_offer[1]).val());
                if(isNaN(market_quantity)) market_quantity = 0;

                if(market_offer[2] >= market_quantity) {
                    total = market_quantity * market_offer[3];
                    total = total.toFixed(3);
                } else if(market_offer[2] < market_quantity){
                    total = market_offer[2] * market_offer[3];
                    total = total.toFixed(3);
                    $(".buyAmount_"+market_offer[1]).val(market_offer[2]);
                }
                var m = $(this).parent().parent().parent().find('td:eq(3) sub');
                if(m.length > 0) {
                    m = m.text().match(/\s([^\/]+)/)[1];
                } else {
                    m = $(this).parent().parent().parent().find('td:eq(3)').text().match(/\s([A-Z]){3}/)[0];
                }
                $("#total_"+market_offer[1]).html(total+' '+m);
                var ammount = $(this).val();
                var price = parseFloat($(this).parent().parent().parent().find("td:eq(3) strong").text().replace(' ', ''));
                var tot = ammount*price;
                if(change !== null) {
                    var button = $(this).parent().find("button").remove();
                    var gold = (tot*change).toFixed(3);
                    $("#gold-tot_"+market_offer[1]).remove();
                    $(this).parent().append($(button).prop('outerHTML')+'<sub id="gold-tot_'+market_offer[1]+'"><br>'+gold+' Gold</sub>');
                }
            });
            $('.buyAmount').trigger('keyup');
        }, country);
    });
    //Compare
    var own = $('.panel-control .btn-group button img').attr('src');
    $('.panel-control .btn-group').before($('.panel-control .btn-group').clone().css('right', '90px').attr('id', 'compare-market'));
    $('#compare-market').find('#findCountry').remove();
    $('#compare-market a').attr('href', '#');
    $('#compare-market ul img[src="'+own+'"]').parent().remove();
    $('.panel-title').append('<strong class="vs501" style="right: 200px;position: absolute;top: 14px;">Compare</strong>');
    $('body').append('<div id="ready"></div>');
    $('#ready').attr('ready', 'false').click(function(){
        var change = parseFloat($(this).attr('ready'));
        $('#compare-offer tbody .star:odd').each(function(){
            var row = $(this).parent().parent().parent();
            var price = parseFloat($('td:nth-child(4) strong', row).text().replace(' ', ''));
            var label = 'Gold / Unit';
            if(parseInt(location.toString().split('/')[6]) < 8) {
                price *= 100;
                label = 'Gold / 100 Units';
            }
            $('td:nth-child(4)', row).append('<br><strong>'+(price*change).toFixed(3)+'</strong> '+label);
        });
    });
    $('#compare-market a').click(function(change){
        $('#compare-offer').remove();
        $('#compare-market button img').attr('src', $(this).find('img').attr('src'));
        $('#compare-market').removeClass('open');
        var country = $(this).find('img').attr('src').replace('.png', '').split('/')[6];
        var cid = t.countries[country];
        var url = location.toString().split('/');
        url[5] = cid;
        url = url.join('/');
        $.ajax(url).done(function(data){
            if($('.vsTable tbody tr', data).length > 0) {
                var offer = $('.vsTable', data).attr('id', 'compare-offer');
                $('.vsTable').before(offer);
                $('.vsTable:eq(0) tbody tr:nth-child(n+6)').remove();
                if($('#ready').attr('ready') == 'false') {
                    $('#ready').attr('ready', 'true');
                } else {
                    $('#ready').click();
                }
            }
        });
        t.getCCExchange(function(change){
            if(change !== null) {
                if($('#ready').attr('ready') == 'false') {
                    $('#ready').attr('ready', change);
                } else {
                    $('#ready').attr('ready', change).click();
                }
            }
        }, cid);
    });
};
eDomPlus.prototype.improveProfiles = function(){
    var t = this;
    if(location.toString().match('profile\/'+edp.id+'\d*'))  {
        // Own
        //Blocks
        $('ul.nav.nav-tabs').append('<li class="vs590"><a href="#tab-blocks" data-target="#tab-blocks" data-toggle="tab" aria-expanded="true">Blocked Users</a></li>');
        $('.tab-content').append('<div class="tab-pane fade" id="tab-blocks"></div>');
        for(var id in t.data.blocks) {
            var user = t.data.blocks[id];
            $('#tab-blocks').append('<li style="list-style: none; margin-bottom: 10px;"><a class="pull-left" href="https://www.edominations.com/en/profile/'+id+'"><img class="vs584" src="/public/upload/citizen/'+id+'.jpg"></a><div style="width: auto; padding: 10px 10px;font-size: 20px;" class="vs585"><a href="https://www.edominations.com/en/profile/'+id+'"> '+user+'</a></div></li>');
        }
        $('#tab-blocks li:last').css('margin-bottom', '0');
        //Remove Friends from list
        $('li.col-md-4').each(function(){
            var id = $(this).find('a').attr('href').split('/').pop();
            $(this).find('.vs585').append('<i class="remove-friend fa fa-close" value="'+id+'" style="float: right; color: red;margin-top: -8px;font-size: 20px;" data-original-title="" title=""></i>');
        });
        $('.remove-friend').tooltip({title: "Click to remove friend", placement: "auto top"}).click(function(){
            var id = $(this).attr('value');
            var formData = new FormData();
            formData.append("remove", id);
            var request = new XMLHttpRequest();
            request.open("POST", "https://www.edominations.com/en/profile/"+t.id);
            request.send(formData);
            $(this).parent().parent().remove();
        });
    } else {
        // Others
        //Blocks
        var button = $($(".col-md-4.text-center").remove().html()).attr("style", "");
        $(".nav.nav-tabs").append('<li id="friend-interact" style="float: right;margin-right: 5px;margin-top: 3px;"></>');
        $("#friend-interact").html(button);
        var id = location.toString().split('/').pop();
        var button,lock = t.data.blocks[id];
        if(!lock) button = 'fa-lock';
        else button = 'fa-unlock-alt';
        $('#friend-interact').after('<li style="float: right;margin-right: 5px;margin-top: 3px;"><button id="block-user" value="'+(lock ? true : false)+'" class="btn btn-'+(lock ? 'info' : 'danger')+' btn-bordered" type="submit"><i class="fa '+button+'" aria-hidden="true"></i></button></li>');
        $('#block-user').tooltip({title: (lock ? 'Unblock' : 'Block')+' user', placement: "auto top"});
        $('#block-user').click(function(){
            var lock = $(this).attr('value') == 'true';
            if(!lock) {
                var nick = $('.vs501').clone();
                $(nick).find('span').remove();
                nick = $(nick).text();
                t.data.blocks[id] = nick;
            } else {
                delete t.data.blocks[id];
            }
            t.store();
            location.reload();
        });
    }
    //Calculator
    $('head').append('<style>#calc {padding: 12px; text-align: center;} #calc b {margin: 0 20px;} #calc label {margin: 0 6px; font-weight: bold;} #calc input[type="checkbox"] {margin-left: 5px;position: relative;top: 2px;text-align: center;} #calc input[type="text"] { padding: 4px;text-align: center;width: 55px;font-size: 11px;margin: 0 5px;} #calc select {padding: 4px;top: 0;margin-left: 2px;} #calc-dmg-table {margin-top: 15px;} #calc-dmg-table > div {display: inline-block; width: 50%;} #calc-weapon-clone {display: none;}</style>');
    $('head').append('<style>@media screen and (max-width:767px){#calc{text-align: left;} #calc label{margin: inherit; font-weight: bold; display: block;} #calc b{margin: 0; display: block;} #calc-dmg-table > div{display: block; width: 100%;}}</style>');
    $('.vs164-3.vs164-11').after('<h5 class="vs586">Damage & Medal Calculator</h5><div class="vs165" id="calc"><label>Hits <input type="text" id="calc-nhits" value="1"></label> <label>Weapon <select id="calc-weapon"><option value="1.0">None</option><option value="1.2">Q1 Weapons</option><option value="1.4">Q2 Weapons</option><option value="1.6">Q3 Weapons</option><option value="1.8">Q4 Weapons</option><option value="2">Q5 Weapons</option></select></label><select id="calc-weapon-clone"><option value="1.0">None</option><option value="1.2">Q1 Weapons</option><option value="1.4">Q2 Weapons</option><option value="1.6">Q3 Weapons</option><option value="1.8">Q4 Weapons</option><option value="2">Q5 Weapons</option><option value="2.2">Q1 Tanks</option><option value="2.4">Q2 Tanks</option><option value="2.6">Q3 Tanks</option><option value="2.8">Q4 Tanks</option><option value="3">Q5 Tanks</option><option value="3.2">Q1 Aircrafts</option><option value="3.4">Q2 Aircrafts</option><option value="3.6">Q3 Aircrafts</option><option value="3.8">Q4 Aircrafts</option><option value="4">Q5 Aircrafts</option><option value="5">Mortars</option></select><label>Round <select id="calc-round"><option value="1">Round 1</option><option value="2">Round 2</option><option value="3">Round 3</option></select></label><label>NE <input type="checkbox" id="calc-ne"></label><label>DS <select id="calc-ds"><option value="1">None</option><option value="0.97">Q1</option><option value="0.95">Q2</option><option value="0.93">Q3</option><option value="0.89">Q4</option><option value="0.85">Q5</option></select></label><label>Booster <select id="calc-booster"><option value="1.0">None</option><option value="1.1">Q1</option><option value="1.2">Q2</option><option value="1.3">Q3</option><option value="1.4">Q4</option><option value="1.5">Q5</option></select></label><div id="calc-dmg-table"><div style="width: 100%;"><b>Damage: <span id="calc-dmg">4,522</span></b></div><div><b>Hits to next Rank: <span id="calc-rank-hits">1,056</span></b></div><div><b>Hits to next TP: <span id="calc-tp-hits">307</span></b></div><div><b>Hits to next TA: <span id="calc-ta-hits">149</span></b></div><div><b>Hits to next TR: <span id="calc-tr-hits">110</span></b></div></div></div>');
    $('#calc-nhits').keyup(function(){
        $(this).val($(this).val().replace(/[^\d]/, ''));
        $(this).trigger('change');
    });
    $('#calc-round').change(function(){
        var select = $('#calc-weapon-clone').clone().attr('class', '#calc-weapon');
        var val = $(this).val();
        if(val < 2) {
            $(select).find('option[value^="2."]').remove();
            $(select).find('option[value^="3"]').remove();
            $(select).find('option[value^="4"]').remove();
            $(select).find('option[value^="5"]').remove();
        } else if(val < 3) {
            $(select).find('option[value^="3."]').remove();
            $(select).find('option[value^="4"]').remove();
        }
        $('#calc-weapon').html($(select).html());
    });
    $('[id^=calc-]').change(function(){
        var booster = parseFloat($('#calc-booster').val());
        var hits = $('#calc-nhits').val();
        if(hits === '') hits = 0;
        hits = parseInt(hits);
        var lvl = parseInt($('.vs164-6.vs164-13').text());
        var ne = 1+($('#calc-ne').is(':checked') ? 0.1 : 0);
        var rank = parseInt($('.vs164-5').attr('src').replace('.png', '').split('/').pop());
        var round = parseInt($('#calc-round').val());
        var str = parseFloat($('.vs164-2').text().replace(',', '').replace('.', ','));
        var weapon = parseFloat($('#calc-weapon').val());
        var ds = parseFloat($('#calc-ds').val());
        var rounddmg = 1;
        if(weapon != 1){
            if(round == 1) {
                rounddmg = 1.5;
            } else if(round == 2) {
                if(weapon <= 2) rounddmg = 0.8;
                else rounddmg = 1.1;
            } else {
                if(weapon <= 2) rounddmg = 0.5;
                else if(weapon <= 3) rounddmg = 0.9;
            }
        }
        var dmg = Math.floor((((((lvl*5)+str)*(1+rank/20)*weapon*ne)*booster)*rounddmg)*ds);
        $('#calc-dmg').text(addCommas(dmg*hits));
        var rankdmg = $($('.vs164-10')[0]).text().replace(/[\s,]/g, '').split('/');
        rankdmg = (parseInt(rankdmg.pop()) - parseInt(rankdmg.pop()))*10;
        $('#calc-rank-hits').text(addCommas(Math.ceil(rankdmg/dmg)));
        var tpdmg = $($('.vs165-5')[0]).text().replace(/[\s,]/g, '').split('/');
        tpdmg = (parseInt(tpdmg.pop()) - parseInt(tpdmg.pop()));
        $('#calc-tp-hits').text(addCommas(Math.ceil(tpdmg/dmg)));
        var tadmg = $($('.vs165-5')[1]).text().replace(/[\s,]/g, '').split('/');
        tadmg = (parseInt(tadmg.pop()) - parseInt(tadmg.pop()));
        $('#calc-ta-hits').text(addCommas(Math.ceil(tadmg/dmg)));
        var trdmg = $($('.vs165-5')[2]).text().replace(/[\s,]/g, '').split('/');
        trdmg = (parseInt(trdmg.pop()) - parseInt(trdmg.pop()));
        $('#calc-tr-hits').text(addCommas(Math.ceil(trdmg/dmg)));
    });
    $('#calc-nhits').trigger('change');
};
eDomPlus.prototype.improveStorage = function(){
    var t = this;
    $('head').append('<style>#sellAmount{float: right;} #sellPrices{margin-bottom: -10px; min-width: 100px; display: block; float: right; margin-left: -30px; margin-top: -7px;} @media screen and (max-width:767px){#sellPrices{min-width: 100px; display: block; margin-left: 35px;} #sellAmount{float: none;} #sellPrice{margin-bottom: 5px;}}</style>');
    $(".sellType").click(function(){
        var item = $(this).attr('id').split("_")[1];
        var q = $(this).attr('id').split("_")[2];
        var name = $(this).attr('id').split("_")[3];
        var elem  = $('.ng-scope').find('img[src="/public/game/items/'+name+'.png"] + main i.star.star-'+q).parent().parent().parent().parent().parent().parent().parent();
        $('#sellAmount').val($(".vs528-4",elem).text());
    });
    $('#sellAmount').prev().css({'padding': '8px 0px 8px 8px', 'display': 'inline-block'});
    $($('table.vs522')[0]).find('tbody tr').each(function(){
        $(this).find('td:eq(1) strong').css({'padding': '8px 0px 8px 8px', 'display': 'inline-block'});
        var ammount = parseInt($('td:eq(1)', this).text());
        if(ammount > 0) {
            $('td:eq(1)', this).append('<input class="form-control reduceOffer reduce_'+ammount+'" name="sell-amount" value="'+ammount+'" maxlength="6" type="text" onkeyup="upkey(event, this)" onkeypress="return checkNumber(\'int\', event)" style="width: 100px;text-align: center;display: inline; float: right; width: 100px; font-weight: bold;">');
            $('form button', this).addClass('reduceOffer-button').attr('type', '').unbind();
        }
    });
    $('.reduceOffer').keyup(function(){
        var max = parseInt($(this).attr("class").split("_")[1]);
        var val = parseInt($(this).val());
        if(val > max) {
            val = max;
            $(this).val(val);
        }
        var label = 'Remove offer';
        if(val !== 0 && !isNaN(val) && val !== max) label = 'Remove '+val;
        $(this).parent().parent().find('form button').text(label);
    });
    $('.reduceOffer-button').click(function(event){
        event.preventDefault();
        var tr = $(this).parent().parent().parent();
        var reduce = parseInt($(tr).find('td:eq(1) input').val());
        var max = parseInt($(tr).find('td:eq(1) strong').text());
        if(reduce === 0 || isNaN(reduce)) reduce = max;
        var price = parseFloat($(tr).find('td:eq(2) strong').text().replace(" ",""));
        var formData = new FormData();
        formData.append('remove-item', $(this).parent().find('input').val());
        var request = new XMLHttpRequest();
        request.onreadystatechange = function(){
            if(this.readyState == 4){
                if(reduce < max){
                    var item = $(tr).find('td:eq(0) img').attr('src');
                    item = t.items[item.split("/").pop().replace(/\.\S+/, '')];
                    var q = parseInt($(tr).find('i.star').attr('class').split('-')[1]);
                    var country = $(tr).find('td:eq(3) img').attr('src');
                    country = country.split("/").pop().replace(/\.\S+/, '');
                    if(item.q.indexOf(q) != -1) {
                        var id = t.countries[country];
                        formData = new FormData();
                        formData.append('sell-amount', max-reduce);
                        formData.append('sell-price', price);
                        formData.append('sell-item', item.id);
                        formData.append('sell-quality', q);
                        formData.append('sell-lic', id);
                        var request = new XMLHttpRequest();
                        request.onreadystatechange = function(){
                            if(this.readyState == 4) setTimeout(function(){window.location = window.location.href.split("#")[0];}, 1e3);
                        };
                        request.open("POST", "https://www.edominations.com/en/inventory");
                        request.send(formData);
                    }
                } else {
                    window.location = window.location.href.split("#")[0];
                }
            }
        };
        request.open("POST", "https://www.edominations.com/en/inventory");
        request.send(formData);
    });
};
eDomPlus.prototype.regionInfo = function(){
    var t = this;
    $.ajax({url: "https://www.edominations.com/en/api2/travel/"}).done(function(data){
        $(".vs151-11").each(function(){
            var region = JSON.parse(data)[0][$(this).attr("href").match(/region\/\d*\/[^\/]*\/(\d*)/)[1]];
            if(region.Defense>0) $(this).after(' <img style="margin-bottom:3px;" src="/public/game/items/defense-system.png" width="18" height="18"> Q'+region.Defense);
        }, function(){});
    });
};
eDomPlus.prototype.sellAlert = function(){
    var t = this;
    $.ajax({url: "https://www.edominations.com/en/inventory"}).done(function(data){
        var offers = {};
        $('tbody tr', $('.table-responsive.vsTable', data)[0]).each(function(){
            var row = this;
            var id = $('input', row).val();
            offers[id] = {};
            offers[id].price = $('td:nth-child(3) strong', row).remove().text();
            offers[id].cc = $('td:nth-child(3)', row).text().trim();
            offers[id].country = $('td:nth-child(4) img', row).attr('src').split('/')[6].replace('.png','');
            offers[id].amount = $('td:nth-child(2)', row).text();
            offers[id].item = $('td:nth-child(1) img', row).attr('src').split('/')[4].replace('.png','').replace('-',' ').ucfirst();
            offers[id].q = $('td:nth-child(1) i', row).attr('class').split('-')[1];
            if(t.data.offers[id]) {
                var dif = t.data.offers[id].amount - offers[id].amount;
                if(dif > 0) {
                    var q = "";
                    if(!offers[id].item.match(/raw/)) q = 'Q'+offers[id].q;
                    t.addAlert(dif+' '+offers[id].item+' '+q+' sold in '+offers[id].country+' for '+offers[id].price+' '+offers[id].cc);
                }
            }
        });
        t.data.offers = offers;
        t.store();
    });
};
eDomPlus.prototype.showAlerts = function(){
    var t = this;
    if(t.data.alerts.length!==0){
        $('.eDp-alert').remove();
        for(var i=0; i< this.data.alerts.length; i++){
            var alert = this.data.alerts[i];
            if(url.match(/alerts/)) {
                $(".list-group").prepend('<li class="list-group-item eDp-alert"><label class="option no-mb"><i class="delete-alert fa fa-close" value="'+i+'"  style="color: red;margin: 0 5px 0 3px;font-size: 20px;"></i><img class="vs574" src="/public/game/icons/notifications.gif" width="22" height="22" style="display: inline;" alt=""> <span class="no-donation" style="color:red;">'+alert.info+'</span></label></li>');
                $(".delete-alert").tooltip({title: "Click to delete notification", placement: "auto top"});
                $(".delete-alert").click(function(){
                    var i = $(this).attr("value");
                    t.data.alerts.splice(i,1);
                    $(this).parent().parent().hide(750);
                    t.store();
                });
            }
        }
        var alerts = $(".l-container-navbar span.fa-bell-o").parent();
        if($(alerts).find(".badge").length===0){
            $(alerts).append('<span class="badge badge-danger badge-xs upbadges">'+t.data.alerts.length+'</span>');
        } else {
            var origs = $(alerts).find(".badge").text();
            $(alerts).attr('origs', origs);
            $(alerts).find(".badge").text(parseInt(origs)+t.data.alerts.length);
        }
        t.updateTitle();
        //Activate Houses
        $('.activate-house').click(function(event){
            event.preventDefault();
            var formData = new FormData();
            formData.append('house-activate', $(this).attr('value'));
            var request = new XMLHttpRequest();
            request.open("POST", 'https://www.edominations.com/en/advanced-buildings');
            request.send(formData);
            $(this).parent().parent().find('.delete-alert').click();
        });
    }
};
eDomPlus.prototype.showBestJobOffer = function(){
    var t = this;
    var url = location.toString().split('/');
    url[url.length-1] = '2';
    $.ajax(url.join('/')).done(function(){
        var country = $('#orgCC img').attr('src').replace('.png', '').split('/');
        var cid = t.countries[country];
        $.ajax('https://www.edominations.com/en/job-market/'+cid+'/1').done(function(data){
            $('#panel-1 thead th:nth-child(3)').append('<br> Best offer: '+$('.vsTable tbody tr:eq(0) td:eq(2)', data).text());
        });
    });
};
eDomPlus.prototype.showGlobals = function(){
    var t = this;
    for(var hash in t.data.globals){
        var msg = t.data.globals[hash];
        if(!msg.hide){
            $("#vsMsg").append('<div id="vsMsg-tab" class="vsMsgID s1500946085 animated bounce edp-global" style="display: block;"><div id="vsMsg-tab-close" class="close" data-id="'+hash+'"><span class="vsMsgClose fa fa-times"></span></div><div id="vsMsg-tab-avatar"><a href="https://www.edominations.com/en/profile/20137"><img src="/public/upload/citizen/20137.jpg" height="70" width="70"></a></div><div id="vsMsg-tab-right"><div id="vsMsg-tab-right-title"><span>Runy96</span> - eDom Plus Developer</div><div id="vsMsg-tab-right-text" style="word-break: normal;"><span>'+msg.txt+'</span></div></div></div>');
        }
    }
    $(".edp-global .close").click(function(){
        t.data.globals[$(this).attr('data-id')].hide=true;
        $(this).parent().remove();
        t.store();
    });
};
eDomPlus.prototype.showLocalCC = function(){
    var t = this;
    var local = $($("td" ,$("tbody tr", $(".vsTable")[0])[0])[3]).text().split("=")[1].trim();
    $.ajax({url: "https://www.edominations.com/en/profile/"+edp.id}).done(function(data){
        data = $("#tab-5 .row", data)[1];
        var flag = $("img[alt="+local+"]", data).attr("src");
        var currency = $("img[alt="+local+"]", data).parent();
        if($(currency).length > 0){
            currency = $(currency).find("span").text();
            $(".col-md-6 h5").append('<span class="vs541" style="float: right; padding: 2 10px; margin: 0px; line-height: 26px;"><b><img src="'+flag+'" width="24" height="24" alt=""> '+currency+' '+local+'</b></span>');
        }
    });
};
eDomPlus.prototype.showMuStats = function(){
    var t = this;
    var batt_id = url.split("/")[5];
    $('head').append('<style>div::-webkit-scrollbar { width: 15px; } div::-webkit-scrollbar-thumb { -webkit-box-shadow: inset 0 0 99px rgba(0,0,0,.2); border: solid transparent; border-width: 6px 4px; } div::-webkit-scrollbar-thumb:vertical { min-height: 40px; } div::-webkit-scrollbar-thumb:horizontal { min-width: 40px; } div::-webkit-scrollbar-thumb:hover { -webkit-box-shadow: inset 0 0 99px rgba(0,0,0,.4); }</style>');
    $.ajax({url: "https://www.edominations.com/en/api2/battle-damage/"+batt_id+"?time="+(new Date().getTime()), cache: false}).done(function(data){
        data = JSON.parse(data);
        var mu = jsonPath(data, "$.["+t.id+"].Unit");
        if(mu) mu = mu[0];
        else mu = 0;
        var html = '<div id="panel-4" class="vs917-1" style="display: none;"><a class="vs917-2" href="#bs-panel" onclick="bsshow(\'panel\', 4); return false;"><img src="/public/game/battle/vs002.png" width="30" height="29" alt=""></a><div class="vs917-15" style="width: 250px;"><a id="muLink" href="https://www.edominations.com/en/military-unit/'+mu+'"><img class="vs917-9" src="/public/upload/group/'+mu+'.jpg" onerror="this.onerror=null;this.src=\'/public/upload/group/0.jpg\';" style="margin-top: -5px;margin-right: 5px;" width="30" height="30" alt=""></a><span class="vs917-5">Select Military Unit (By ID)</span><input id="muSelect" style="margin-left: 10px; width: 50px;" placeholder="'+mu+'"></div>';
        html += '<div class="vs917-3" style="overflow-y: auto; max-height: 380px; margin-top: 45px; padding-top: 0px;"><table width="100%" height="30" border="0" align="center" cellpadding="0" cellspacing="0" style="padding-top: 0px;"><tbody><tr class="vs917-4"><td class="text-center" height="42" colspan="5"></tbody></table></div>';
        html += '<div class="vs917-10" style="overflow-y: auto; max-height: 380px; margin-top: 45px; padding-top: 0px;"><table width="100%" height="30" border="0" align="center" cellpadding="0" cellspacing="0" style="padding-top: 0px;"><tbody><tr class="vs917-4"><td class="text-center" height="42" colspan="5"></tbody></table></div>';
        html += '<a class="" href="#bs-panel" onclick="bsshow(\'panel\', 4); return false;"><div class="vs917-19">Close</div></a></div>';
        $('#battleLog').next().remove();
        $('#battleLog').after('<script type="text/javascript">function bsshow(what, x) {if (what == \'panel\') {for (var i=1; i<=4; i++) {if(i!=x){document.getElementById("panel-"+i+"").style.display = \'none\';}}var obiekt1 = document.getElementById("panel-"+x+"");if(obiekt1.style.display == \'block\') {document.getElementById("panel-"+x+"").style.display = \'none\';} else {document.getElementById("panel-"+x+"").style.display = \'block\';}}}</script>');
        $('#panel-3').after(html);
        $('#panel-4').append($($(".vs917-13")[0]).prop('outerHTML'));
        $('#panel-4').append($($(".vs917-14")[0]).prop('outerHTML'));
        $('.vs911.vs911-3').after('<div class="vs911 vs911-4" style="right: 162px;"><a class="vs911-1" href="javascript:;" onclick="bsshow(\'panel\', 4); return false;" data-toggle="tooltip" data-placement="top" title="" data-original-title="View MU Top"></a></div>');
        $('[data-toggle="tooltip"]').tooltip({trigger: 'hover'});
        t.showTop(mu, data);
        var timeOut = null;
        $("#muSelect").keyup(function(){
            if (timeOut !== null) clearTimeout(timeOut);
            timeOut = setTimeout(function() {
                globalTimeout = null;
                t.showTop($("#muSelect").val(), data);
            }, 500);
        });
    });
};
eDomPlus.prototype.showTop = function(id, data){
    var t = this;
    $("#panel-4 .vs917-4").remove();
    var i, html, img, length = 0, att_dmg = 0, att_hit = 0, def_dmg = 0, def_hit = 0;
    if(id === "") {
        id = jsonPath(data, "$.["+this.id+"].Unit");
        if(id) id = id[0];
        else id = 0;
    }
    for(i = 0; i < 8; i++){
        html = '<tr class="vs917-4"><td class="text-center" height="42" colspan="5"><span class="vs917-7">...</span></td></tr>';
        $("#panel-4 .vs917-3 table tbody").append(html);
        $("#panel-4 .vs917-10 table tbody").append(html);
    }
    if(!isNaN(id) && (function(x) { return (x | 0) === x; })(parseFloat(id))){
        $("#muLink").attr("href", "https://www.edominations.com/en/military-unit/"+id);
        $("#muLink img").attr("src", '/public/upload/group/'+id+'.jpg');
        var fighters = jsonPath(data, "$..[?(@.Unit=="+id+")]");
        if(fighters) {
            $("#panel-4 .total").remove();
            $("#panel-4 .vs917-4").remove();
            fighters.sort(function(a,b){return b.DMG - a.DMG;});
            var att = jsonPath(fighters, '$.[?(@.SIDE=="attack")]');
            var def = jsonPath(fighters, '$.[?(@.SIDE=="defense")]');
            if(att) length = att.length;
            if(def) length = (length > def.length ? length : def.length);
            for(i = 0;  i < length; i++){
                if(att && att[i]) {
                    img = '/public/upload/citizen/'+att[i].ID+'.jpg';
                    html = '<tr class="vs917-4"><td class="text-center" width="20"><span class="vs917-7">'+(parseInt(i)+1)+'</span></td><td class="text-center" width="50" height="45"><img class="vs917-9" src="'+img+'" onerror="this.onerror=null;this.src=\'/public/upload/citizen/0.jpg\';" width="40" height="40" alt=""></td><td><a href="https://www.edominations.com/en/profile/'+att[i].ID+'"><span class="vs917-8">'+att[i].Name+'</span></a></td><td class="text-center" width="50"><span class="vs917-6">'+att[i].Hits+'</span></td><td class="text-center" width="90"><span class="vs917-6">'+addCommas(att[i].DMG)+'</span></td></tr>';
                    att_dmg += att[i].DMG;
                    att_hit += att[i].Hits;
                    $("#panel-4 .vs917-3 table tbody").append(html);
                }
                if(def && def[i]) {
                    img = '/public/upload/citizen/'+def[i].ID+'.jpg';
                    html = '<tr class="vs917-4"><td class="text-center" width="90"><span class="vs917-6">'+addCommas(def[i].DMG)+'</span></td><td class="text-center" width="50"><span class="vs917-6">'+def[i].Hits+'</span></td><td class="text-right"><a href="https://www.edominations.com/en/profile/'+def[i].ID+'"><span class="vs917-8">'+def[i].Name+'</span></a></td><td class="text-center" width="50" height="45"><img class="vs917-9" src="'+img+'" onerror="this.onerror=null;this.src=\'/public/upload/citizen/0.jpg\';" width="40" height="40" alt=""></td><td class="text-center" width="20"><span class="vs917-7">'+(parseInt(i)+1)+'</span></td></tr>';
                    def_dmg += def[i].DMG;
                    def_hit += def[i].Hits;
                    $("#panel-4 .vs917-10 table tbody").append(html);
                }
            }
            $("#panel-4 .vs917-3").prepend('<table class="total" width="100%" height="30" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-bottom:17px;"><tbody><tr class="vs917-4"><td class="text-center" width="20"><span class="vs917-7">&nbsp;</span></td><td class="text-center" width="50" height="45"><img class="vs917-9" src="/public/upload/group/'+id+'.jpg" onerror="this.onerror=null;this.src=\'/public/upload/group/0.jpg\'" width="40" height="40" alt=""></td><td><a href="https://www.edominations.com/en/military-unit/'+id+'"><span class="vs917-8">Total</span></a></td><td class="text-center" width="50"><span class="vs917-6">'+att_hit+'</span></td><td class="text-center" width="90"><span class="vs917-6">'+addCommas(att_dmg)+'</span></td></tr></tbody></table>');
            $("#panel-4 .vs917-10").prepend('<table class="total" width="100%" height="30" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-bottom:17px;"><tbody><tr class="vs917-4"><td class="text-center" width="90"><span class="vs917-6">'+addCommas(def_dmg)+'</span></td><td class="text-center" width="50"><span class="vs917-6">'+def_hit+'</span></td><td class="text-right"><a href="https://www.edominations.com/en/military-unit/'+id+'"><span class="vs917-8">Total</span></a></td><td class="text-center" width="50" height="45"><img class="vs917-9" src="/public/upload/group/'+id+'.jpg" onerror="this.onerror=null;this.src=\'/public/upload/group/0.jpg\'" width="40" height="40" alt=""></td><td class="text-center" width="20"><span class="vs917-7">&nbsp;</span></td></tr></tbody></table>');
        }
    }
};
eDomPlus.prototype.showValue = function(){
    var t = edp;
    t.getCCExchange(function(change){
        change = parseFloat(change);
        $('#sellPrice').parent().find('span:gt(0)').remove();
        $("#sellPrice").after('<div id="sellPrices"><span id="sellTot">Total</span><br><span>Tax</span> <span id="sellCC"></span><br><span id="sellGold">Gold</span><div></div></div>');
        $("#sellAmount, #sellPrice").keyup(function(){
            if(change !== Infinity) t.calcPrices(change);
        });
        $(".sellType, .sellTax").click(function(){
            var q = $("#sellQuality").attr("value");
            var item = $("#sellItem").attr("value");
            var country = $("#sellLic").attr("value");
            if(country !== 0 && item !== 0 && q !== 0){
                var market = "https://www.edominations.com/en/market/"+country+"/"+item+"/"+q+"/1";
                $.ajax({url: market}).done(function(data){
                    var offers = $($(".table-responsive tbody tr", data)[0]);
                    if(offers.length>0){
                        var price = $("strong",$("td",offers)[3]).text().replace(" ", "");
                        $("#sellPrice").attr("value", price).val(price);
                        t.calcPrices(change);
                    }
                });
            }
        });
    });
};
eDomPlus.prototype.sortDo = function(){
    if($('#add-odrder').length>0){
        $('#add-order option:eq(0), #add-order2 option:eq(0)').attr('data-text', '00000');
        $('#add-order').attr('order', $('#add-order').val());
        $('#add-order2').attr('order', $('#add-order2').val());
        $('#add-order').find('option').sort(function(a,b){
            a = $(a).attr('data-text').split('-').pop().trim();
            b = $(b).attr('data-text').split('-').pop().trim();
            if(a < b) return -1;
            if(a > b) return 1;
            return 0;
        }).appendTo('#add-order');
        $('#add-order').val($('#add-order').attr('order'));
        $('#add-order2').find('option').sort(function(a,b){
            if($(a).attr('value') == '0') a = $(a).attr('data-text').split('-').pop().trim();
            else a = $(a).text().split('-').pop().trim();
            if($(b).attr('value') == '0') b = $(b).attr('data-text').split('-').pop().trim();
            else b = $(b).text().split('-').pop().trim();
            if(a < b) return -1;
            if(a > b) return 1;
            return 0;
        }).appendTo('#add-order2');
        $('#add-order2').val($('#add-order2').attr('order'));
    }
};
eDomPlus.prototype.updateBattlefield = function(update_button){
    var t = this;
    $('body').append('<div id="helper" style="display: none;"></div>');
    $("#helper").load(location.href+"?time="+(new Date().getTime())+" .vs917-1>*","", function(response){
        $('div.vs900 #panel-3').html($('#panel-3', response).html());
        $('div.vs900 #panel-2').html($('#panel-2', response).html());
        $('div.vs900 #panel-1').html($('#panel-1', response).html());
        var round = $("#panel-3 > div ").length;
        var att_dmg = parseFloat($("#panel-3 #round-"+round+" div.vs917-17").text().replace(/,/g, ''));
        var def_dmg = parseFloat($("#panel-3 #round-"+round+" div.vs917-18").text().replace(/,/g, ''));
        var tot = att_dmg+def_dmg;
        var att_pct = (att_dmg*100/tot).toFixed(2);
        if(isNaN(att_pct)) att_pct = 50;
        var def_pct = (100-att_pct).toFixed(2);
        $("#battleValA").empty();
        $("#battleValA").append(""+ att_pct +" %");
        $("#battleValD").empty();
        $("#battleValD").append(""+ def_pct +" %");
        $("#battleProcA").animate({"width": att_pct +"%"}, 1000);
        $("#battleProcD").animate({"width": def_pct +"%"}, 1000);
        if(t.data.settings.improveBattlefield.active) {
            $('#panel-1').prepend(t.update_button);
            $('#panel-2').prepend(t.update_button);
            $('#panel-3').prepend(t.update_button);
            $('.updateBtf').click(function(){t.updateBattlefield();});
        }
        $('#helper').remove();
    });
};
eDomPlus.prototype.updateBtfOnHit = function(){
    var t = this;
    setTimeout(function(){$('#battleFight').click(function(){t.updateBattlefield();});},50);
    $('#loadItems').bind("DOMSubtreeModified", function(){
        if ($('#loadItems').is(':empty')){
            setTimeout(function(){$('#battleFight').click(function(){t.updateBattlefield();});},50);
        }
    });
};
eDomPlus.prototype.updateGlobals = function(){
    var t = this;
    t.load();
    var msg = "";
    $.ajax({url: "https://bitbucket.org/Rubensei/edom-plus/raw/"+t.branch+"/globals.js", cache: false}).done(function(data){
        if(typeof data == 'string' && data.isJSON()){
            data = JSON.parse(data);
            for(var j in t.data.globals){
                if(data.indexOf(t.data.globals[j].txt) == -1) delete t.data.globals[j];
            }
            for(var i = 0; i < data.length; i++){
                msg = data[i];
                var test = msg.indexOf('#Test#') != -1;
                if(!test || test && t.id == 20137){
                    if(test) msg = msg.replace("#Test#","");
                    if(!t.data.globals[msg.hash()]){
                        t.data.globals[msg.hash()] = {};
                        t.data.globals[msg.hash()].txt=msg;
                        t.data.globals[msg.hash()].hide=false;
                    }
                }
            }
            t.store();
            t.showGlobals();
        }
    });
};
eDomPlus.prototype.updateHouses = function(callback){
    var t = this;
    this.data.last_hcheck = this.ed_day;
    $.ajax({url:"https://www.edominations.com/en/advanced-buildings"}).done(function(data){
        var actives = ['',false, false, false, false, false];
        $(".table-responsive tbody tr", data).each(function(){
            if($("img",this).attr("src") == "/public/game/items/house.png"){
                var q = $("i.star", this).attr("class").split("-")[1];
                var stock = $("strong",$("td", this)[2]).text();
                if(!t.data.houses[q]) t.data.houses[q] = {};
                if($(".label", this).length>0){
                    t.data.houses[q].active=true;
                    if($(".label-danger", this).length>0){
                        var h = $(".label", this).text().match(/[^\d](\d+)[^\d]/)[1];
                        var d1 = new Date(serverdate).getUTCDate();
                        var d2 = new Date(Number(serverdate)+h*1000*60*60).getUTCDate();
                        t.data.houses[q].days = d2 - d1;
                    } else {
                        var days = parseInt($(".label", this).text().match(/[^\d](\d+)[^\d]/)[1]);
                        if(t.data.houses[q].days < days) t.data.houses[q].alerted = false;
                        t.data.houses[q].days=days;
                    }
                    actives[q] = true;
                } else {
                    if(!actives[q]){
                        t.data.houses[q].active=false;
                        t.data.houses[q].days=0;
                    }
                }
                if(stock !== "" && stock > t.data.houses[q].stock) t.data.houses[q].alerted=false;
                t.data.houses[q].stock=(stock===""?0:stock);
            }
        });
        t.store();
        callback();
    });
};
eDomPlus.prototype.updateTitle = function(){
    var t = this;
    if($('.badge.badge-danger.badge-xs.upbadges').length > 0){
        var alerts = 0;
        $('.badge.badge-danger.badge-xs.upbadges').each(function(){
            alerts += parseInt($(this).text());
        });
        $('title').text('('+alerts+') '+$('title').attr("orig"));
    } else {
        $('title').text($('title').attr("orig"));
    }
};

var url = location.toString();
var edp = new eDomPlus();
if($("#sidebar-nav").length>0){
    if(edp.data.settings.removeBackground.active) $('body').css('background-image', 'none');
    edp.checkVersion();
    if(edp.data.settings.alertHouses.active) edp.checkHouses();
    if(edp.data.settings.sellAlert) edp.sellAlert();
    edp.updateGlobals();
    if(!url.match(/org/)){
        if(edp.data.settings.dayStats.active) edp.dayStats();
        if(url.match(/\/article\//)){
            if(edp.data.settings.improveArticle.active) edp.improveArticle();
        }
        if(url.match(/battlefield/)){
            if(edp.data.settings.fastTravel.active) edp.fastTravel();
            if(edp.data.settings.improveBattlefield.active) edp.improveBattlefield();
            if(edp.data.settings.updateBtfOnHit.active) edp.updateBtfOnHit();
            if(edp.data.settings.showMuStats.active) edp.showMuStats();
        }
        if(url.match(/\/companies/)){
            if(edp.data.settings.improveCompanies.active) edp.improveCompanies();
        }
        if(url.match(/^https:\/\/www\.edominations\.com\/(?:$|\w{2}\/index)/)){
        }
        if(url.match(/inventory/)){
            if(edp.data.settings.improveStorage.active) edp.improveStorage();
            if(edp.data.settings.showValue.active) edp.showValue();
        }
        if(url.match(/\/market\/\d+\/[^0]\d*\//)){
            if(edp.data.settings.improveMarkets.active) edp.improveMarkets();
        }
        if(url.match(/\/market-compare\/\d+\/\d+/)){
            edp.globalCompare();
        }
        if(url.match(/\/market\/\d+\/4/)){
            if(edp.data.settings.improveFoodMkt.active) edp.improveFoodMkt();
        }
        if(url.match(/military-unit/)){
            edp.sortDo();
        }
        if(url.match(/monetary-market/)) {
            if(edp.data.settings.autofillMaxMoney.active) edp.autofillMaxMoney();
            if(edp.data.settings.showLocalCC.active) edp.showLocalCC();
        }
        if(url.match(/profile/)){
            edp.improveProfiles();
        }
        if(url.match(/wars/)) {
            if(edp.data.settings.battleFilter.active) edp.battleFilter();
            if(edp.data.settings.battleInfo.active) edp.battleInfo();
            if(edp.data.settings.regionInfo.active) edp.regionInfo();
        }
    } else {
        if(url.split('/')[7] == 6) {
            edp.showBestJobOffer();
        }
    }
}

function addCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function changeCss(className, classValue) {
    var cssMainContainer = $('#css-modifier-container');
    if (cssMainContainer.length === 0) {
        cssMainContainer = $('<div id="css-modifier-container"></div>');
        cssMainContainer.hide();
        cssMainContainer.appendTo($('body'));
    }

    var classContainer = cssMainContainer.find('div[data-class="' + className + '"]');
    if (classContainer.length === 0) {
        classContainer = $('<div data-class="' + className + '"></div>');
        classContainer.appendTo(cssMainContainer);
    }

    classContainer.html('<style>' + className + ' {' + classValue + '}</style>');
}

function next(obj, key){
    var found = 0;
    for(var k in obj){
        if(found){ return k; }
        if(k == key){ found = 1; }
    }
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function getKey(object, value) {
    return Object.keys(object).find(key => object[key] === value);
}

function waitFor(selector, callback){
    var poller1 = setInterval(function(){
        $jObject = jQuery(selector);
        if($jObject.length < 1){
            return;
        }
        clearInterval(poller1);
        callback($jObject)
    },100);
}

String.prototype.isJSON = function() {
    try {
        JSON.parse(this);
    } catch (e) {return false;}
    return true;
};
String.prototype.hash = function () {
    var strlen = this.length;
    if (strlen === 0) return 0;
    for (var i=0,nHash = 0, n; i<strlen; ++i) {
        nHash = ((nHash<<5)-nHash)+this.charCodeAt(i);
        nHash = nHash & nHash;
    }
    return nHash >>> 0;
};
String.prototype.ucfirst = function(){
    return this.charAt(0).toUpperCase() + this.substr(1);
};